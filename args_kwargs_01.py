# -*- coding: utf-8 -*-


#args - kwargs (2):
    

##Misal olarak 3 tana değeri toplayabilirsin ancak bir sürü değeri nasıl toplayacağız?
##Yani bir fonksiyonda kaç tane değer gireceğimizden emin değilsek "*" opeatöründen faydalanabiliriz.

def plus(*args): ## Args bir listedir ve tüm parametreleri bir liste olarak alır.
    plus = 1
    for arg in args:
        plus += arg
    return plus

print(plus(1,2,4,3,2,4,3))        

def ortalam(*args):  #istediğimiz kadar sayının ortalamasını alalım
    return sum(args) / len(args)

print(ortalam(3,5,4,3,6,54,3,454,3534,534,))




def func(**kwargs): #Sonsuz keywords oluşturma vardır, sözlük olarak saklar.
    print(kwargs)
    
func(ad = "Ali" , soyad = "Luna", yas = 34)


def func2(zorunlu_parametre, *args,**kwargs):
   
    print(zorunlu_parametre)
    
    for arg in args:   #Args durumunun içinde bir arg yarat ve her defasında bunu ekrana ver
        print(arg)
    
    for kwarg in kwargs:
        print(kwarg)      
    
print(func2("Bu",1,2,3,4,5,6,ad="Ali",yas=34))  

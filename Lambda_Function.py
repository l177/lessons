# -*- coding: utf-8 -*-

#Lambda Fonksiyonları (İfadeleri)
    #İsmi olmayan fonksiyonlar yapmamıza  yarar


#Nasıl Kullanılır?:

topla = lambda a , b : a + b
kup_al = lambda x : x ** 3

# 1-) Topla olarak bir değişken atadık
# 2-) "lambda" yazdığımız yerin sağına içine alacak değeri yazıyoruz
# 3-) ":" kısmından sonra ise return edeceği durumu yazıyoruz

print(topla(1,-2))
print(kup_al(2))

#*args aldığını düşünerek yapalım:
    
Genel_Toplam = lambda *args : sum(args)
print(Genel_Toplam(1,1)) 

#Print() içeresinde çalıştıralım:

print(( lambda x ,y : x-y ) (9,3))



#Kullanım Alanları:
    
    #Bir liste olduğunu ve sıralamanın sıfırıncı indekse doğru yapıldığını düşünelim biz bunu birinci endekse göre sıralanmış şekilde
        #çalıştırmak istiyoruz


liste = [("Ahmet" , 29) , ("Mehmet", 19) , ("Luna" , 20)]
liste.sort(key = lambda x : x[1]) #burada lambda kullanarak listeyi sıralama işlemini 1.indekse göre yapmasını istedik. Key fonk ile.
print(liste)

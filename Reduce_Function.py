# -*- coding: utf-8 -*-

#Reduce Fonksiyonu:
    
from functools import reduce

        #Parametre olarak bir tane fonk. bir tane list alıyor.
        #Farkı :  Map ve Filter: List döndürürken, Reduce bize sadece bir tane değer döndürür.
        
Liste = [1,2,3,4,5]
 
sonuc1 = reduce(lambda x , y : x + y , Liste)
print(sonuc1)
sonuc2 = reduce(lambda x , y : x * y , Liste)
print(sonuc2)



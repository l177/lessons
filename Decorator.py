# -*- coding: utf-8 -*-

#Python - Decorator Kullanımı:
        #Kendisi de bir fonksiyondur.
        #Halihazırda var olan fonksiyonlarımıza özellikler eklemeye yarar.
        #Misal bir programda 100 tane fonksiyon çalışsın ve biz de ne kadar sürede çalıştıklarını bilmek isteyelim.



#Zaman hesaplama işlemi:
import time  

def zaman_hesapla(fonk):
    def wrapper(*args,**kwargs):
        Baslangic = time.time()
        fonk(*args,**kwargs)
        Bitis = time.time()
        print(f"işlem {Bitis - Baslangic} saniye sürdü.")
    return wrapper
@zaman_hesapla    
def Kareleri_Al(liste):
    for i in liste:
        print(i*i)

@zaman_hesapla        
def Küpleri_Al(liste):
    for i in liste:
        print(i**3)

Kareleri_Al(range(10000))
Küpleri_Al(range(100000))
# -*- coding: utf-8 -*-

#Map Fonksiyonu:
    #Yazdığımız kodun daha kısa ve düzenli olmasını sağlayan fonksiyondur.
    #Parametre olarak fonk ve list alır. Listedeki elemenanları fonk'da yerine koyar ve elde ettiği sonuçlardan bir tane yeni bir list
    #oluşturur.
    

# liste = [1,3,23,23,234,34,234,32]

# def kare_al(x):
#     return x * x

#  #Listedeki elemanların tümünü bu fonksiyona yedirelim.
 
# liste2 = list(map(kare_al , liste)) #list(), set(), tuple()
 
# print(liste2)
 

                    ##LAMBDA İLE KULLANIMI:
                    # liste = [1,2,3,6,7,9]
                    # liste_2 = list(map(lambda x : x * x, liste))
                    # liste_3 = list(map(lambda x : x + x , liste ))

                    # print(liste_2)
                    # print(liste_3)
                    
                    
#BİR FONKSİYON İKİ TANE LIST ALSIN:
    
liste_1 = [1,2,3,4,5]
liste_2 = [6,7,8,9]

liste_3 = list(map(lambda x , y : x + y,liste_1 , liste_2)) 
print(liste_3)


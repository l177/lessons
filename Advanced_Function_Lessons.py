# -*- coding: utf-8 -*-
"""
Created on Tue Aug 24 17:27:32 2021

@author: Asus
"""

#İç İçe Fonksiyonlar.

def out_func():
    print("Dış Fonksiyon Çalışıyor")
    
    def  inside_func():
        print("İç Fonksiyon Çalışıyor")
        
    
    print("Fonksiyon Sonlandı")
    inside_func() #Burada çağırma yapıyoruz.


#out_func()

def Hesaplama(x = "Girilmedi",*args,**kwargs):
    def kare_al(a = "Girilmedi"):     
        for arg in args:
            a *= arg
        return a ** 2
        
    return print(kare_al(x))
    
    print("İşlem Sonlandı")
    
Hesaplama(17)          

            

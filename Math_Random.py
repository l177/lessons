# -*- coding: utf-8 -*-

#Random ve Math Modülleri:
    
import random 
import math

#print(random.__dir__()) #HELP
#print(help(random)) #HELP 

print(random.random()) #Random: 0 ila 1 arasında bir sayı oluşturur (0.5088936070740724)

#Daha geniş bir aralıkta rastgele sayı oluşturmak istiyorsam:
print(random.uniform(5,10)) #a : float , b : float (5.907753888707202)

#Eğer float cinsinden değil de integer cinsinden oluşturmak istiyorsan:
print(random.randint(10, 100)) # (70 )  a : integer , b : integer

#Listedeki belirli sayılar arasından da seçebilir.
Liste = [1,3,4,6,7,8,9,0]
print(random.choice(Liste))
print(random.choice(range(10)))  #Range: Belirli seçilen aralık durumu.

      

#Eğer belirli sayı kadar rastgele seçim yapmak istiyorsan

print(random.sample(Liste , k = 3))  #sequence: Can be a list, tuple, string, or set.
                                     #k: An Integer value, it specify the length of a sample.
                                     #Returns: k length new list of elements chosen from the sequence.
                                             #https://www.geeksforgeeks.org/python-random-sample-function/#


#Bir listeyi rastgele shuffle'lamak istiyorsan:

random.shuffle(Liste) #Note: This method changes the original list, it does not return a new list.

print(Liste) #[3, 4, 1, 6, 7, 0, 9, 8]

#********************************************************************************************************************************************#
#MATH MODÜLÜ:
    
print(help(math))

print(math.factorial(10))
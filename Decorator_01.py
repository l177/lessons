# -*- coding: utf-8 -*-

#Decorator _01_

import time

def zamani_hesapla(zaman):
    def wrapper(*args,**kwargs):
        Baslangic = time.time()
        sonuc = zaman(*args,**kwargs)
        Bitis = time.time()
        print(f"{Baslangic - Bitis} Saniye sürdü")
        return sonuc
        
    return wrapper
        
        
        
@zamani_hesapla        
def kareleri_al(liste):
    sonuc = []
    for i in liste:
        sonuc.append(i * i)
    return sonuc 

@zamani_hesapla 
def küp_al_al(liste):
    sonuc = []
    for i in liste:
        sonuc.append(i ** 3)
    return sonuc 

print(küp_al_al(range(100)))

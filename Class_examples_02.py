    

#Inheritance - Python[Class]

class Worker:
    raise_rate = 1.2
    
    def __init__(self,name,lastname,salary):
        self.name = name
        self.lastname = lastname 
        self.salary = salary 
        self.email = name + lastname + "@company.com"
    
    def Show_information(self):
        return "Name: {} Lastname: {} Salary: {} Email: {}".format(self.name,self.lastname,self.salary,self.email)
    
     

class Software_engineer(Worker):
    def __init__(self, name, lastname, salary, soft_lang):
        super().__init__(name, lastname, salary) 
        self.soft_lang = soft_lang

                                                                                                                       
    def Show_information_eng(self):
            return f"""
        
    Name: {self.name}
    Lastname: {self.lastname}
    Salary: {self.salary}
    Main Software Language: {self.soft_lang}"""
    
class Admin(Worker):
    def __init__(self, name, lastname, salary,worker=None):
        super().__init__(name,lastname,salary)
        
        if worker == None:
            self.worker = []
        else:
            self.worker = worker
            
    def add_worker(self,worker):
        if worker not in self.worker:
            self.worker.append(worker)
            
    def delete_worker(self,worker):
        if worker in self.worker:
            self.worker.remove(worker)
    
    def Show_Worker(self):
        for worker in self.worker:
            print(str(Worker.Show_information))
            
        
worker1 = Worker("Elena", "DXD", 1500)
print(worker1.Show_information())

Admin1 = Admin("Bll","DUU",160000)
Admin1.add_worker(worker1)
print(str(Admin1.Show_information()))
# -*- coding: utf-8 -*-

#*args ve *kwargs #Function

def Kuvvet_al(x,y): #Sıralamanın önem aldığı argümanlara: "Positional arguments" denir. Katı kurallara dayanır (5) yapamazsın.
    return x**y

print(Kuvvet_al(3, 4)) #Ayrıyeten Positional arguments'lerde ekstra olarak üçüncü bir değer giremezsin çünkü parametre sınırı kadar
                        #değer yazılır.


def Bilgi_Goster(ad,soyad,yas = "Girilmedi"): #Yas = "Girilmedi" yaptığımız zaman eğer herhangi bir değer atamazsak direkt olarak 
                                                #yanındaki uyarıyı girecektir. Bu fonksiyonlara ise Keyword Arguments diye geçer.
    return f"Ad: {ad}, Soyad: {soyad} Yaş: {yas}"

print(Bilgi_Goster("Doğuhan", "İlter" ))#Burada Yaş Girişi Yapılmadı

def Bilgi_Goster2(ad,soyad = "Girilmedi",yas = "Girilmedi"):
    return f"Ad: {ad}, Soyad: {soyad} Yaş: {yas}"

#Misal olarak ben "ad" değerini girdim ve üçüncü parametre olarak "yaş" değerini girdim ve soyad paramatresini atladım; şu şekilde yapılacak.

print(Bilgi_Goster2("Doğuhan", yas = 23)) #Bu şekilde özel olarak keywords belirtirsem "soyad" parametresini direkt olarak atlayacaktır.



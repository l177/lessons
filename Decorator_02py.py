# -*- coding: utf-8 -*-

# Property , Getter , Setter, Deleter  (Decorator)
#https://github.com/sinanurun/Python_ileri_seviye/tree/master/Python_oop_(nesne_yonelimli_python)

class calisan:

    def __init__(self,ad,soyad):
        self.ad = ad
        self.soyad = soyad
        
    @property
    def Eposta(self):
        self.eposta = self.ad + self.soyad + "@sirket.com"
        return self.eposta
    @property
    def tamad(self):
        return "İsmi : {}  Soyismi : {}".format(self.ad , self.soyad)

personel1 = calisan("Doğuhan","İlter")

personel1.ad = "Luna"

print(personel1.ad)
print(personel1.Eposta)
print(personel1.tamad)
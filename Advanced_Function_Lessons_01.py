# -*- coding: utf-8 -*-
#Fonksiyonlardan fonksiyon döndürme

def  fonk(x):
    return x*x

a = fonk #Fonksiyonları da parametreler gibi değişkenlere atayabiliriz.
#print(a(5))


#*************************************************************************#

def islem_sec(islem):
    
    def toplama(*args):
        toplama = 0
        for arg in args:
            toplama += arg
        return toplama        
    
    def carpma(*args):
        carpmas = 1
        for arg in args:
            carpmas *= arg
        return carpmas
        
    def ortalama(*args):
        return sum(args) / len(args)
    
    if islem == "toplama":
        return toplama
    
    elif islem == "carpma":
        return carpma        
    
    elif islem =="ortalama":
        return ortalama
    
    else:
        print("Böyle bir uygulama bulunamadı")


a = islem_sec("toplama")
print(a(25,52))

b = islem_sec("carpma")

print(b(12,12))

# -*- coding: utf-8 -*-


#Dateime ve Time modülü
#Tarih ve Zaman  

#Gün - Ay - Yıl olarak ilerler.

from datetime import date

Bugun = date.today() #Yıl / Ay / Gün olarak döndürür.

Kendimiz = date(2019 , 10 , 15) #Kendimiz adına da time oluşturabiliriz.

print(Bugun - Kendimiz) #Aralarında dört işlem yapılabiliyor.

#Eğer aradaki farkı görmek istersem:
Zaman_Araligi =  Bugun - Kendimiz
print(Zaman_Araligi.days)  #Zaman aralığını gün cinsinden söyler.

#Aralarında "Hangisi daha büyük zaman dilimi?" olarak kıyaslamalar yapılabiliyor:
print(Bugun < Kendimiz) #FALSE
print(Kendimiz < Bugun) #TRUE

#Tutulan hücrelere teker teker ulaşabiliyoruz:
print(Bugun.year) #Bugun.__getattribute('year')
print(Bugun.month)
print(Bugun.day)

#Haftanın kaçıcı günü olduğunu da söyletebiliyoruz (0 indeksli):
print(date.weekday(Bugun))

#Daha görseli arttırılmış bir şekilde bastırmak istiyorsan:
print(date.ctime(Bugun))

from datetime import time
    
zaman = time(2 , 1) #Zamanı direkt olarak kendimiz aktarabiliyoruz
print(zaman)

zaman.hour #Date'te ki gibi spesifik ulaşımlar yapabiliyoruz
zaman.minute #Date'te ki gibi spesifik ulaşımlar yapabiliyoruz
zaman.second #Date'te ki gibi spesifik ulaşımlar yapabiliyoruz

#Hepsini toplu bir şekilde de yapabiliriz:
import datetime
dt = datetime.datetime(2020,10,21,2,4)

    #1.Hücre = Yıl
    #2.Hücre = Ay
    #3.Hücre = Gün              OLARAK TANIMLANMIŞTIR.
    #4.Hücre = Saat
    #5.Hücre = Dakika 
    # Definition : dt(year, month, day[, hour[, minute[, second[, microsecond[,tzinfo]]]]])

print(dt)

import time


#Şu anın zamanını da alabiliriz.

print(time.time())  #Cinsi farklı bir şekilde bastırabilir.
print("************************************************************")

Baslangic = time.time()
time.sleep(10)
Bitis = time.time()
print(f"{Baslangic - Bitis} Saniye sürdü")



#Zaman hesaplama gibi durumlarda kullanılıyor.
# import time

# def zamani_hesapla(zaman):
#     def wrapper(*args,**kwargs):
#         Baslangic = time.time()
#         sonuc = zaman(*args,**kwargs)
#         Bitis = time.time()
#         print(f"{Baslangic - Bitis} Saniye sürdü")
#         return sonuc
        
#     return wrapper
# -*- coding: utf-8 -*-

#Class

class calisan():
    #Zam durumunu yapın: 
    zam_durumu = 1.1
    calisan_kisi_sayisi = 0
    def __init__(self,isim,maas):
        self.isim = isim
        self.maas = maas
#Her nesne oluşturulduğunda çalışan sayısını arttırmak istiyorsak:
        calisan.calisan_kisi_sayisi +=1
#Böyle yaparak her nesne oluşturduğumuzda ekstra olarak +1 olarak artacak.        
        
        
    def Zamdurumu(self):
        return f"Zam durunuz{self.maas * calisan.zam_durumu}"
    
    #Class Methods nedir?: Herhangi bir nesne oluşturmadan da kullanılabilir.
    
    @classmethod
    def kisi_sayisini_soyle(cls):
        return cls.calisan.calisan_kisi_sayisi
    
        
calisan1 = calisan("Ali",23000)
calisan2 = calisan("Mehmet",2000)

print(calisan1.__dict__) #dict : dictionary'den gelir ve değerleri bir sözlük gibi döndürür
print(calisan1.Zamdurumu()) # Burada 1.1 olarak aldık çünkü class yapısını öyle tanıttık.

calisan.zam_durumu = 1.4 #Burada ikinci personelin zam değerini değiştirip farklı bir değer tanıttık.
print(calisan2.Zamdurumu())

print(calisan.calisan_kisi_sayisi)  #
#Bu print kısmını en alta koymamızın sebebi programların yukarıdan aşağıya okuması ve bize toplam
#Değer göndermesini istememiz.

print(calisan.kisi_sayisini_soyle)



